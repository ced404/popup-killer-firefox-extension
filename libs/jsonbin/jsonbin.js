// https://jsonbin.io/ API wrapper

// TODO: error handling
// TODO: collections api
// TODO: versions api

const JSONbin = (function() {
	const Config = {
		BIN_API: "https://api.jsonbin.io/b",
		VERSIONS_API: "https://api.jsonbin.io/e",
		secretKey: ""
	};

	// Get bin versions
	const versions = function(id) {
		return new Promise(function(resolve, reject) {
			let req = new XMLHttpRequest();
			req.withCredentials = false;

			req.open("GET", `${Config.VERSIONS_API}/${id}/versions`, true);
			req.setRequestHeader("Content-type", "application/json");
			req.setRequestHeader("secret-key", Config.secretKey);

			req.onreadystatechange = function() {
				if (req.readyState == XMLHttpRequest.DONE) {
					if (req.status == 200) {
						resolve(JSON.parse(req.responseText));
					} else {
						reject(Error(req.statusText));
					}
				}
			};

			req.onerror = function() {
				console.error("req.responseText", req.responseText);
				reject(Error(req.responseText));
			};

			req.send();
		});
	};

	// Create bin
	const create = function(data = [{}]) {
		data = JSON.stringify(data);

		return new Promise(function(resolve, reject) {
			let req = new XMLHttpRequest();
			req.withCredentials = false;

			req.open("POST", `${Config.BIN_API}`, true);
			req.setRequestHeader("Content-type", "application/json");
			req.setRequestHeader("secret-key", Config.secretKey);

			req.onreadystatechange = function() {
				if (req.readyState == XMLHttpRequest.DONE) {
					if (req.status == 200) {
						resolve(req.responseText);
					} else {
						reject(Error(req.statusText));
					}
				}
			};

			req.onerror = function() {
				reject(Error("Network Error"));
			};

			req.send(data);
		});
	};

	// Update Bin
	const update = function(id, data) {
		if ("string" !== typeof data) data = JSON.stringify(data);

		return new Promise(function(resolve, reject) {
			let req = new XMLHttpRequest();
			req.withCredentials = false;

			req.open("PUT", `${Config.BIN_API}/${id}`, true);
			req.setRequestHeader("Content-type", "application/json");
			req.setRequestHeader("secret-key", Config.secretKey);

			req.onreadystatechange = function() {
				if (req.readyState == XMLHttpRequest.DONE) {
					if (req.status == 200) {
						resolve(JSON.parse(req.responseText));
					} else {
						reject(Error(req.statusText));
					}
				}
			};

			req.onerror = function() {
				reject(Error("Network Error"));
			};

			req.send(data);
		});
	};

	// load bin
	const read = function(id, version = "latest") {
		return new Promise(function(resolve, reject) {
			let req = new XMLHttpRequest();
			req.withCredentials = false;

			req.open("GET", `${Config.BIN_API}/${id}/${version}`, true);
			req.setRequestHeader("Content-type", "application/json");
			req.setRequestHeader("secret-key", Config.secretKey);

			req.onreadystatechange = function() {
				if (req.readyState == XMLHttpRequest.DONE) {
					if (req.status == 200) {
						resolve(req.responseText);
					} else {
						reject(Error(req.statusText));
					}
				}
			};

			req.onerror = function() {
				reject(Error("Network Error"));
			};

			req.send();
		});
	};

	return {
		read: read,
		create: create,
		update: update,
		versions: versions
	};
})();

// let newData = {
// 	"sample": "hello world ;)",
// 	"timestamp": new Date().getTime()
// };
//
// JSONbin.update (newData).then (
// 	function (results) {
// 		let response = JSON.parse (results);
// 		console.info ('success', response);
// 		window.open (`https://api.jsonbin.io/b/${response.parentId}/${response.version}`);
// 	},
// 	function (results) {
// 		console.error ('error', results);
// 	},
// );
