// TODO: implement blacklist update from JSONbin.io saved in storage.sync

const popupKiller = (function() {
	"use strict";

	let State = {
		// load default blacklist, overriden if a storage.local blaclikst is found
		config: window.pk_config
	};

	const isEmptyObject = obj =>
		Object.keys(obj).length === 0 && obj.constructor === Object;

	// update blacklist
	const updateBlacklist = () => {
		// Load the blacklist specified in config.js
		let localBlacklist = {
			JSON_BIN_ID: State.config.JSON_BIN_ID,
			JSON_BIN_VERSION: State.config.JSON_BIN_VERSION
		};

		JSONbin.read(State.config.JSON_BIN_ID, State.config.JSON_BIN_VERSION).then(
			response => {
				// error if no blacklist is found on JSONbin
				if (!response || null === response) {
					return console.error(
						"Empty / no blacklist found!",
						State.config.JSON_BIN_ID,
						State.config.JSON_BIN_VERSION
					);
				}

				// paths to regex patterns
				let blacklistPaths = JSON.parse(response);
				let blacklist = blacklistPaths.map(path => new RegExp(path));

				// Add the blacklist array
				localBlacklist.YETT_BLACKLIST = blacklist;

				// update storage
				browser.storage.local
					.set({
						localBlacklist: localBlacklist
					})
					.then(
						function(results) {
							console.log(
								"Local blacklist updated, changes will take effect on page reload.",
								results
							);
							return true;
						},
						function(err) {
							console.error(
								"Local storage set error! blacklist not updated.",
								err
							);
							return false;
						}
					);
			},
			error => {
				console.error("Error loading JSONbin", error);
				return null;
			}
		);
	};

	// Get blacklist from extension local storage
	const loadLocalBlacklist = async () => {
		browser.storage.local.get("localBlacklist").then(
			function(results) {
				// if (isEmptyObject(localBlacklist)) return null;
				console.log("loadLocalBlacklist", results.localBlacklist);
				window.YETT_BLACKLIST = results.localBlacklist.YETT_BLACKLIST;
				return results.localBlacklist;
			},
			function() {
				return null;
			}
		);
	};

	const init_interactive = () => {
		console.log("init_interactive");
	};

	const init_complete = () => {
		console.log("init_complete");
	};

	const init = async () => {
		console.log("💣 popup killer init!", State);

		// return updateBlacklist();

		// load local storage config
		let localBlacklist = await loadLocalBlacklist();

		// then set YETT_BLACKLIST
		// TODO: check bin version and id
		// if (localBlacklist && null !== localBlacklist.YETT_BLACKLIST) {
		// 	window.YETT_BLACKLIST = localBlacklist.YETT_BLACKLIST;
		// }
		// // or get remote blacklist then create the local storage config (async)
		// else updateBlacklist();

		document.onreadystatechange = () => {
			if (document.readyState == "interactive") init_interactive();
			if (document.readyState == "complete") init_complete();
		};
	};

	return {
		init: init
	};
})();

// popupKiller.init();
