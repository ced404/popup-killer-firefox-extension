# Cookie Banner Killer

This extension will prevent cookie banners/consent libraries from loading using Yett.

- https://github.com/snipsco/yett
- https://medium.com/snips-ai/how-to-block-third-party-scripts-with-a-few-lines-of-javascript-f0b08b9c4c0

Try https://cookieconsent.osano.com/demos/ ;)

## TODO

- [ ] implement blacklist update from JSONbin.io
- [ ] save blacklist in in storage.sync
- [ ] add option page to add scripts to blacklists
- [ ] add script blocking toggle button
- [ ] implement user private JSONbin blacklist
- [ ] blocked script information/log


## Changelog

### next version

- load blacklist from jsonbin.io
- save blacklist in the extension local storage

### v0.1

- Basic script block with Yett.js
- Yett blacklist regex patterns in `config.js`
- Yett 0.1.9 loaded from `manifest.json`