// Default blacklist
window.YETT_BLACKLIST = [
	/consent\.cmp\.oath\.com/,
	/cmp3p/,
	/cmpStub/,
	/quantcast\.mgr\.consensu\.org/,
	/quantcast-choice\.js/,
	/cookieconsent(.*)?\.js/,
	/cmp.uniconsent\.mgr\.consensu\.org/,
	/wp-content\/plugins\/cookie-notice/,
	/cookiebar/,
	/cookieMessage/,
	/cookie-consent/,
	/secureprivacy\.ai/,
	/cookiebot\.com/,
	/jqueryCookieGuard/,
	/jquery-eu-cookie-law-popup/,
	/jquery\.cookiecuttr\.js/,
	/jquery\.cookie/,
	/termly\.io/,
	/tags\.tiqcdn\.com/,
	/cookie-law-info/,
	/consentmanager\.net/,
	/cookieControl/,
	/optanon-alert(.*)?/,
	/cdn\.cookielaw\.org/,
	/cdn\.tagcommander\.com\/privacy/,
	/didomi/,
	/sdk\.privacy\-center\.org/,
	/woopic\.com/,
	/RGPD/,
	/jcookies/,
	/cookienotice/
];

// Public JSONbin ID and version
window.pk_config = {
	JSON_BIN_ID: "5d84c85fa3ecb0483233ac8d",
	JSON_BIN_VERSION: 4
};
